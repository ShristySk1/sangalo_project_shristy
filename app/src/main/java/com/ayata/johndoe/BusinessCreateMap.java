package com.ayata.johndoe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class BusinessCreateMap extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap map;
    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_create_map);
        SupportMapFragment mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        View headerLayout = findViewById(R.id.map_include);

        TextView headerText=(TextView)headerLayout.findViewById(R.id.tmn_textView11);
        headerText.setText("Select Address");
        btn=findViewById(R.id.btn_map);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(BusinessCreateMap.this,BusinessCreate2.class);
                startActivity(i);

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        LatLng home=new LatLng(27.708480, 85.305539);
        map.addMarker(new MarkerOptions().position(home).title("home"));
        map.moveCamera(CameraUpdateFactory.newLatLng(home));
    }
}